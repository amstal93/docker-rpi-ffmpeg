# Docker image for FFmpeg on the Raspberry Pi

[![build status](https://gitlab.com/fradelg/docker-rpi-ffmpeg/badges/master/build.svg)](https://gitlab.com/fradelg/docker-rpi-ffmpeg/commits/master)

[FFmpeg](https://github.com/FFmpeg/FFmpeg) compiled from source with x264 for ARM

## Tags

- 3.2.4 ([Dockerfile])(https://gitlab.com/fradelg/docker-rpi-ffmpeg/blob/master/3.2/Dockerfile)
- 3.3.1, latest ([Dockerfile])(https://gitlab.com/fradelg/docker-rpi-ffmpeg/blob/master/3.2/Dockerfile)

## Features

- compiled from source in a Raspberry Pi 3
- based on Debian Jessie
- x264 enabled
- x265 enabled

## Building

You can find this image in [Docker Cloud](https://hub.docker.com/r/fradelg/rpi-ffmpeg) or build it by yourself cloning this repository and executing:

```
docker build -t <your_username>/rpi-ffmpeg .
```

## Testing

```
docker run -it fradelg/rpi-ffmpeg ffmpeg -h
```

```
docker-compose up
```
